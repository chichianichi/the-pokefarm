# The Poke-Farm
<p>Entregable para el Bootcamp,desarrollo Frontend de páginas dinámicas con React.<br>
La característica principal es la comunicación con una RESTful API, así traer información y presentarla en pantalla.
 <br>
 <br>
 En la app vamos a tener dos vistas, una de Home y otra con 
 el nombre The-Poke-Farm. Ambas son accesibles desde botones en 
 el apartado de navegación.

<h2>El proyecto cuenta con las siguientes características y funcionalidades:</h2>

<ul>
    <li>Desarrollado con React</li>
    <li>Peticiones a RESTful API ([PokéAPI](https://pokeapi.co))</li>
    <li>Vistas con HTML.</li>
    <li>Estilos con CSS</li>
    <li>Reactstrap</li>
    <li>Elementos dinámicos</li>  
    <li>Enrutamiento</li>         
</ul>



