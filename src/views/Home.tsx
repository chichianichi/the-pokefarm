import React, { FC } from "react";
import {
  Card,
  CardBody,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle,
  Col,
  Container,
  Row,
} from "reactstrap";
import "./styles/Home.css";

const Home: FC = () => {
  return (
    <Container style={{ justifyContent: "space-between" }}>
      <Row id="title-row">
        <Col sm={{ size: 8, offset: 4 }}>
          <h1>Alexis Hernández</h1>
        </Col>
      </Row>
      <Row id="subtitle-row">
        <Col sm={{ size: 8, offset: 5 }}>
          <h3>About Me</h3>
        </Col>
      </Row>
      <Row>
        <Col sm="12" md="6" lg="4" style={{ paddingBottom: "15px" }}>
          <Card className="card-content">
            <CardImg
              top
              width="338"
              src="https://i1.wp.com/www.mugsnoticias.com.mx/wp-content/uploads/427-20.png?fit=800%2C503&ssl=1"
              alt="Card image cap"
            />
            <CardBody>
              <CardTitle tag="h5">Computer Science Degree Student</CardTitle>
              <CardSubtitle tag="h6" className="mb-2 text-muted pb-3">
                At the university I learned about
              </CardSubtitle>
              <CardText>
                <ul>
                  <li>Java with MVC pattern</li>
                  <li>SCRUM and Software Engineering</li>
                  <li>Relational Databases with MySQL</li>
                </ul>
              </CardText>
            </CardBody>
          </Card>
        </Col>
        <Col sm="12" md="6" lg="4" style={{ paddingBottom: "15px" }}>
          <Card className="card-content">
            <CardImg
              top
              width="100%"
              src="https://www.creativefabrica.com/wp-content/uploads/2018/12/Knowledge-icon-by-back1design1-1-580x381.png"
              alt="Card image cap"
            />
            <CardBody>
              <CardTitle tag="h5">My Knowledge</CardTitle>
              <CardSubtitle tag="h6" className="mb-2 text-muted pb-3">
                On my Own
              </CardSubtitle>
              <CardText>
                <ul>
                  <li>HTML and CSS</li>
                  <li>Bootstrap</li>
                  <li>Git,Github and GitLab</li>
                  <li>Javascript(Beginner)</li>
                  <li>Vue JS, Angular and React JS basics</li>
                </ul>
              </CardText>
            </CardBody>
          </Card>
        </Col>
        <Col sm="12" md="6" lg="4" style={{ paddingBottom: "15px" }}>
          <Card className="card-content">
            <CardImg
              top
              width="100%"
              src="https://yeny2803queenita.files.wordpress.com/2013/06/music.jpg"
              alt="Card image cap"
            />
            <CardBody>
              <CardTitle tag="h5">Favorite Music</CardTitle>
              <CardSubtitle tag="h6" className="mb-2 text-muted pb-3">
                My Favorite Artists
              </CardSubtitle>
              <CardText>
                <ul>
                  <li>Cafe Tacuba</li>
                  <li>Michael Jackson</li>
                  <li>Fernando Delgadillo</li>
                  <li>Rodrigo y Gabriela</li>
                  <li>Daft Punk</li>
                </ul>
              </CardText>
            </CardBody>
          </Card>
        </Col>
        <Col sm="12" md="6" lg="4" style={{ paddingBottom: "15px" }}>
          <Card className="card-content">
            <CardImg
              top
              width="100%"
              src="https://medcitynews.com/uploads/2019/05/Screen-Shot-2019-05-17-at-4.01.26-PM-600x396.png"
              alt="Card image cap"
            />
            <CardBody>
              <CardTitle tag="h5">Favorite Videogames</CardTitle>
              <CardSubtitle tag="h6" className="mb-2 text-muted pb-3">
                Through My Life
              </CardSubtitle>
              <CardText>
                <ul>
                  <li>Final Fantasy 7</li>
                  <li>Nier Automata</li>
                  <li>Pokemon</li>
                  <li>Super MArio World</li>
                  <li>Final Fantasy XV</li>
                </ul>
              </CardText>
            </CardBody>
          </Card>
        </Col>
        <Col sm="12" md="6" lg="4" style={{ paddingBottom: "15px" }}>
          <Card className="card-content">
            <CardImg
              top
              width="100%"
              src="https://luakabop.com/portal/wp-content/uploads/silviobg-1.jpg"
              alt="Card image cap"
            />
            <CardBody>
              <CardTitle tag="h5">I'm a Guitarist</CardTitle>
              <CardSubtitle tag="h6" className="mb-2 text-muted pb-3">
                Favorite Genders
              </CardSubtitle>
              <CardText>
                <ul>
                  <li>Trova y Canto Nuevo</li>
                  <li>Rock en Español</li>
                  <li>Pop</li>
                  <li>Boleros</li>
                  <li>Rock & Roll</li>
                </ul>
              </CardText>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Home;
