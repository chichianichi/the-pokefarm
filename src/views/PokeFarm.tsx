import React, { FC, useEffect, useState } from "react";
import { getPokemon } from "../services/pokemon";
import CardPokemon from "../components/Card/CardPokemon";
import { Button, Col, Container, Row } from "reactstrap";

const PokeFarm: FC = () => {
  const [pokemonData, setPokemonData] = useState([]);
  const initialUrl = "https://pokeapi.co/api/v2/pokemon";
  let randomNumbers: any = [];

  const generateRandom = async () => {
    for (let i = 0; i < 12; i++) {
      randomNumbers[i] = Math.floor(Math.random() * (899 - 1) + 1);
    }

    let pokemon: any = await Promise.all(
      randomNumbers.map(async (index: any) => {
        let pokemonRecord = await getPokemon(`${initialUrl}/${index}`);
        return pokemonRecord;
      })
    );
    setPokemonData(pokemon);
  };

  useEffect(() => {
    async function fetchData() {
      let ranPokemon: any = await generateRandom();
    }
    fetchData();
  }, []);

  return (
    <Container>
      <Row id="title-row" className="pb-4">
        <Col sm={{ size: 8, offset: 4 }}>
          <h1>The Poke-Farm App</h1>
        </Col>
      </Row>
      <Row id="subtitle-row">
        <Col>
          <Button
            outline
            color="warning"
            size="lg"
            block
            onClick={generateRandom}
          >
            New Random Pokemon
          </Button>{" "}
        </Col>
      </Row>
      <Row>
        {pokemonData.map((pokemon, index) => {
          return <CardPokemon key={index} pokemon={pokemon} />;
        })}
      </Row>
    </Container>
  );
};

export default PokeFarm;
