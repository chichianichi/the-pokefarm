import React from "react";
import "./style.css";
import { Card, CardImg, Col } from "reactstrap";
import typeColors from "./CardPokemonColors";

type CardProps = {
  pokemon: any;
};

const CardPokemon = ({ pokemon }: CardProps) => {
  return (
    <Col sm="6" md="3" style={{ paddingBottom: "15px" }}>
      <Card>
        <div>
          <CardImg
            top
            width="100%"
            src={pokemon.sprites.front_default}
            alt="pokemonImage"
          />
        </div>
        <div>
          <h3>{pokemon.name.toUpperCase()}</h3>
          <p>
            {pokemon.types.map((type: any) => {
              return (
                <div
                  className="Card_type"
                  style={{ backgroundColor: typeColors[type.type.name] }}
                >
                  {type.type.name}
                </div>
              );
            })}
          </p>
          <p>
            <b>No:</b> {pokemon.id}
          </p>
          <p>
            <b>Height:</b> {pokemon.height}
          </p>
          <p>
            <b>Ability:</b> {pokemon.abilities[0].ability.name}
          </p>
        </div>
      </Card>
    </Col>
  );
};

export default CardPokemon;
