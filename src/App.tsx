import "./App.css";
import {
  Button,
  Collapse,
  Container,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  Row,
} from "reactstrap";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
  Link,
} from "react-router-dom";
import Home from "./views/Home";
import PokeFarm from "./views/PokeFarm";
import { useState } from "react";

function App() {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Router>
      <Container fluid={true} id="menu">
        <Container>
          <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/">The Poke-Farm</NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
              <Nav className="mr-auto" navbar>
                <NavItem>
                  <NavLink>
                    <Link to="/" className="btn">
                      <Button outline color="warning">
                        Home
                      </Button>{" "}
                    </Link>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink>
                    <Link to="/pokefarm" className="btn">
                      <Button outline color="warning">
                        Poke-Farm app
                      </Button>{" "}
                    </Link>
                  </NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        </Container>
      </Container>

      <Container className="App">
        <Switch>
          <Route path="/pokefarm">
            <PokeFarm />
          </Route>
          <Route path="/">
            <Home />
          </Route>
          <Redirect to="/" />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
